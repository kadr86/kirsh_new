class AddIsPublicToQuestions < ActiveRecord::Migration
  def change
  	add_column :questions, :is_public, :integer
  end
end
