# REQUIRE
require 'rvm/capistrano'
set :rvm_type,        :system
set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"")

require 'bundler/capistrano'

# SETTINGS
set :application, "kirsh"
set :deploy_to,   "/home/admin/www/#{application}"

set :domain, "178.250.247.98"
set :user,   "admin"

set :rails_env, "production"

server domain, :web, :app, :db, primary: true

set :use_sudo, false
set :keep_releases, 2

set :shared_children, shared_children + %w{public/uploads}

set :scm,                   :git
set :branch,                "master"
set :deploy_via,            :export
set :git_enable_submodules, 1
set :repository,            "/home/admin/www/git/#{application}.git"
set :local_repository,      "ssh://#{user}@#{domain}/home/admin/www/git/#{application}.git"

# TASKS
# before 'deploy:setup', 'rvm:install_ruby'

after 'deploy:update_code', :roles => :app do
  run "rm -f #{current_release}/config/database.yml"
  run "ln -s #{deploy_to}/shared/config/database.yml #{current_release}/config/database.yml"
  run "sudo chown -R $(whoami):www-data #{deploy_to}"
  run "sudo chmod -R 777 #{current_release}/public/sale_objects"
  run "/bin/cp -rf #{previous_release}/public/sale_objects/ #{current_release}/public/sale_objects/"
end

desc 'copy ckeditor nondigest assets'
task :copy_nondigest_assets, roles: :app do
  run "cd #{current_release} && #{rake} RAILS_ENV=#{rails_env} ckeditor:create_nondigest_assets"
end
after 'deploy:assets:precompile', 'copy_nondigest_assets'


after 'deploy', 'deploy:migrate', 'deploy:cleanup'

after "deploy:update", "deploy:cleanup" 

namespace :deploy do
  task :restart do
    run "sudo /etc/init.d/unicorn_kirsh upgrade"
  end
  task :force_restart do
    run "sudo /etc/init.d/unicorn_kirsh stop"
    run "sudo /etc/init.d/unicorn_kirsh start"
  end
  task :start do
    run "sudo /etc/init.d/unicorn_kirsh start"
  end
  task :stop do
    run "sudo /etc/init.d/unicorn_kirsh stop"
  end
end