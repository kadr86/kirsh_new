Kirsh::Application.routes.draw do


  get "help_informations/index"
  get "help_informations/new"
  get "help_informations/edit"
  get "select_objects/new"
  get "select_objects/edit"
  mount Ckeditor::Engine => '/ckeditor'
  root 'sites#index'


  get 'signin' => 'sessions#new'
  get 'signout' => 'sessions#destroy'
  get 'admin' => 'admins#index'
  get 'yamail' => 'sites#yamail'

  resources :sessions, :only => [:new, :create, :destroy]
  resources :user_accounts
  resources :effective_rieltors
  resources :personals
  resources :client_requests do
    post :load_request, on: :collection
    post :check_phone, on: :collection
  end
  resources :client_request_without_objects do
    post :load_request, on: :collection
  end
  resources :sale_objects do
    member do
      patch :hot_offer
      patch :avans
      patch :sold
      patch :expired
      get :photo_delete
    end
    resources :photo, only: [:create, :destroy]
    post :getDistricts, on: :collection
    post :filter, on: :collection
  end
  resources :sites do
   collection do
    get :about
    get :question_answer
    get :contacts
    get :zalog
    get :buyout
    get :kredit
    get :agents
    get :own_biznes
    get :developer
    get :owner_komerch
    get :search
    post :search_request
    get :request_user
    get :request_advert
    post :send_request
    post :send_request_advert
    post :question_answer_create
    post :consultant
    post :search_map
    get :map_site
    get :map_site_print
    get :document_example
   end
   get :object_detail, on: :member
   get :print, on: :member
  end
  resources :abouts
  resources :answers
  resources :developers
  resources :owner_komerches
  resources :kredits
  resources :zalogs
  resources :buyouts
  resources :bisnes_selfs
  resources :agents
  resources :regions
  resources :news_on_sites do
    get :admin_index, on: :collection
    get :public, on: :member
  end
  resources :main_pages
  resources :districts
  resources :seos
  resources :top_banners
  resources :papers do
    get :site_index, on: :collection
  end
  resources :tests
  resources :select_objects
  resources :help_informations

end
