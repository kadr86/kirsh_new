# encoding: utf-8

class ClientRequest < ActiveRecord::Base
  belongs_to :client
  belongs_to :sale_object
  belongs_to :personal  

  accepts_nested_attributes_for :client
  scope :treatment, -> {where(status: 1)}	
  scope :accompanied, -> {where(status:[2,3])}

  validates  :personal_id, :sale_object_id, presence: true

  after_save do
  	client = Smspilot.new "H69SXX2EXAP3E2TI92W03GTT955PQ9ZLFSQXSO51C1NH65K57TZIUDF88YTPK024"    
    result = client.send_sms(self.id, 'KIRSH', self.personal.mobile_phone.delete('-'), "#{self.client.name}, тел. #{self.client.phone}, #{self.sale_object.street_type}#{self.sale_object.street} #{self.sale_object.object_type} #{self.sale_object.object}#{self.sale_object.letter} #{self.sale_object.block_type unless self.sale_object.block.blank?}#{self.sale_object.block}#{',' unless self.comment.blank?} #{self.comment}")
  end

end
