class TopBanner < ActiveRecord::Base
	mount_uploader :banner, BannerUploader

	scope :is_public, ->{where is_public: 1}
end
