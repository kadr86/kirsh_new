class SaleObject < ActiveRecord::Base
  attr_accessor :photo1, :photo2, :photo3, :photo4, :photo5, :photo6, :photo7, :photo8, :photo9, :photo10
	has_many :photos, :dependent => :destroy 
  # accepts_nested_attributes_for :photos, allow_destroy: true   
  belongs_to :personal
  has_many :client_requests     
  
  scope :presentation, -> {where(status: 2)}
  scope :current_objects, -> {where(status: 3)}
  scope :olds_objects, -> {where(status: 7)} 
  scope :hot_offer, -> {where(hot_offer: 1)}

  scope :contract, -> {where(agreement: 0, status:[3,4,5])}	
	scope :agreement, -> {where(agreement: 1, status:[3,4,5])}

  # scope :no_rooms, ->(offer,square_from, square_to, price_from, price_to, region, object){where(avans: 0, sold: 0, expired: 0, status: offer, general_square: square_from..square_to, price: price_from..price_to).where("region like ? AND structure_type like ?", region, object).order("street ASC")}

  # scope :rooms, -> (offer, rooms, square_from, square_to, price_from, price_to, region, object){where(avans: 0, sold: 0, expired: 0,status: offer, rooms_count: rooms, general_square: square_from..square_to, price: price_from..price_to).where("region like ? AND structure_type like ?", region, object).order("street ASC")}

  # scope :district, -> (district){where("district like ?", district).order("street ASC")}

  # scope :street, -> (street){where("street like ?", street).order("street ASC")}
  
  scope :is_public, -> {where.not(personal_id: 'null')}
  
  validates :region, :locality, :street, :begin_contract, :price,  presence: true

  def photo_upload photo, object_id

    dir = Rails.root.join('public', 'sale_objects',object_id.to_s)
    
    path_original = Rails.root.join('public', 'sale_objects',object_id.to_s,"original_#{photo.original_filename}") 
    path_small = Rails.root.join('public', 'sale_objects',object_id.to_s,"small_#{photo.original_filename}") 
    path_hot = Rails.root.join('public', 'sale_objects',object_id.to_s,"hot_#{photo.original_filename}") 
    path_medium = Rails.root.join('public', 'sale_objects',object_id.to_s,"medium_#{photo.original_filename}") 
    
    Dir.mkdir dir unless Dir.exist? dir
    Dir.chdir dir    

    File.open(path_original, "wb") { |file| file.write(photo.read)  }
  
    Devil.with_image(path_original.to_s) {|img| img.resize2(86,77); img.save(path_small.to_s, :quality => 85) }
    Devil.with_image(path_original.to_s) {|img| img.resize2(200,150); img.save(path_hot.to_s, :quality => 85) }
    Devil.with_image(path_original.to_s) {|img| img.resize2(488,341); img.save(path_medium.to_s, :quality => 85) }
  end
end
