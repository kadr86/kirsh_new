class NewsOnSite < ActiveRecord::Base
	mount_uploader :image, NewsImageUploader

	scope :is_public, ->{where(is_public: 1)}
	validates :title,:anons, :description, presence: true
end
