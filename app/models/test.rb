class Test 
	include ActiveModel::Model
	
	attr_accessor :fio, :email, :phone, :question1, :question2, :question3, :question4, :question5, :question6

	validates :fio, :email, :phone, presence: true

	def summ
		question1.to_i+question2.to_i+question3.to_i+question4.to_i+question5.to_i+question6.to_i
	end

end