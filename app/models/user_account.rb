class UserAccount < ActiveRecord::Base
  
  attr_accessor :password

  has_one :personal

  validates :login, :email, :password, :password_confirmation, presence: true
  validates :email, length: {minimum: 3, maximum: 254},
                    uniqueness: true
                    # format: {with: /\A([\A@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }                    

  validates :login, uniqueness: true    
  validates :password, length: {minimum: 5, maximum: 20}, confirmation: true
  

  before_save :encrypt_password, if: :password?   

  def has_password?(password)
    self.encrypted_password == encrypt(password)
  end

  def self.authenticate(login, password)
    user = find_by_login(login)
    return nil  if user.nil?
    return user if user.has_password?(password)
  end

  def self.authenticate_with_salt(id, cookie_salt)
    user = find_by_id(id)
    (user && user.salt == cookie_salt) ? user : nil
  end
  
private

  def encrypt_password                  
      self.salt = make_salt if new_record?
      self.encrypted_password = encrypt(password)       
  end

  def encrypt(string)      
      secure_hash("#{salt}--#{string}")
  end

  def make_salt
      secure_hash("#{Time.now.utc}--#{password}")
  end

  def secure_hash(string)
      Digest::SHA2.hexdigest(string)
  end

  def password?
      password.present?
  end
end
