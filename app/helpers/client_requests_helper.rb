# encoding: utf-8

module ClientRequestsHelper

	def getClientRequestStatus
		{"обратился" => 1, "сопровождается" => 2, "купил в КИРШ" => 3, "купил не в КИРШ" => 4, "прервал сопровождение" => 5}	
	end

	def getClientRequestWitoutObject
		{"обратился" => 1, "сопровождается" => 2}
	end

	def getClientRequestStatusName id
		 getClientRequestStatus.key(id)
	end

	def getAccess
		{"Риэлтор" =>1, "Офис" =>2, "Все" =>0}
	end

	def getFinancing
		["накопление","ипотека","обмен","обмен с доплатой","обмен с профитом"]
	end

	def getFloor
		["любой","1","2","последний","не 1","не последний"," не 1, не последний"]
	end

	def getTypeWitoutObject
		["Городской дом", "Загородный дом", "Квартира", "Комната", "Коммер. помещение", "Земля", "Гараж", "Квартира гост. типа", "Дача", "Новостройка","Займ","Срочный выкуп","Мат. капитал", "Ипотека"]
	end

end
