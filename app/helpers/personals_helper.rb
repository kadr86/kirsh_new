# encoding: utf-8

module PersonalsHelper

	def getPosition
		{"директор" => 1,
		 "директор по франчайзингу" => 2,
		 "зам. директора по маркетингу" => 3,
		 "зам. директора по работе с клиентами" => 4,
		 "зам. директора по франчайзингу" => 5,
		 "зам. директора по общим вопросам" => 6,
		 "старший консультант отдела франчайзинга" => 7,
		 "консультант отдела франчайзинга" => 8,
		 "старший консультант отдела новаций" => 9,
		 "консультант отдела новаций" => 10,
		 "старший консультант отдела коммерческой недвижимости" => 11,
		 "консультант отдела коммерческой недвижимости" => 12,
		 "консультант по юридическим вопросам" => 13,
		 "старший консультант по недвижимости" => 14,
		 "консультант по недвижимости" => 15,
		 "менеджер по персоналу" => 16,
		 "координатор офиса" => 17,
		 "консультант по инвестиционной деятельности" => 18,
		 "заместитель директора по инвестиционной деятельности" => 19,
		 "офис-менеджер" => 20,
		 "старший менеджер отдела новаций" => 21,
		 "менеджер отдела новаций" => 22,
		 "консультант по сопровождению сделок с недвижимостью" => 23,
		 "специалист по внутреннему контролю" => 24,
		 "менеджер по развитию" => 25,
		 "менеджер по рекламе" => 26,
		 "менеджер по работе с зарубежной недвижимостью" => 27
		}
	end

	def getPositionName id
		getPosition.key id
	end

	def getJobStatus
		{"работает" => 1,"перевелся" => 2,"уволился" => 3,"уволен" => 4,"без права восст." => 5}
	end

	def getJobStatusName id
		getJobStatus.key id
	end
	
	def getRang
		{"Риэлтор" => 1,"Администратор агенства" => 2,"Главный администратор" => 3}
	end

	def getInichials name
		name =name.split(" ")		
		"#{name[0][0].swapcase}#{'. '+name[1][0].swapcase + '.' unless name[1].nil? }"		
	end

	def birthday
		image_tag 'birthday.png'
	end

	def birthday?(id)
		persona = Personal.find id		
		unless persona.birthday.nil? 
			return true  if persona.birthday.month == Time.current.month && persona.birthday.day >= Time.current.day && persona.birthday.day <= (Time.current+1.weeks).day 		
		end	
		return false
	end

	def getBirthday id
	 persona = Personal.find id		
		unless persona.birthday.nil? 
			birthday  if persona.birthday.month == Time.current.month && persona.birthday.day >= Time.current.day && persona.birthday.day <= (Time.current+1.weeks).day 		
		end
	end

end
