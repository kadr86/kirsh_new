# encoding: utf-8

module SitesHelper
	
	def logo
		image_tag 'head_logo.png'
	end

	def vk
		image_tag 'vk.png'
	end

	def twitter
		image_tag 'twitter.png'
	end

	def facebook
		image_tag 'facebook.png'
	end

	def odnoklasniki
		image_tag 'odnoklasniki.png'
	end

	def instagram
		image_tag 'instagram.png'
	end

	def home_service
		image_tag "home_service.png"	
	end
	
	def email_service
		image_tag "email_service.png"		
	end

	def site_map
		image_tag "site_map_service.png"	
	end

	def getHotOffers
		@hot_offers = SaleObject.hot_offer
	end

	def getMinSquare
		SaleObject.minimum(:general_square).round
	end

	def getMaxSquare
		SaleObject.maximum(:general_square).round
	end	

	def getMinPrice
		SaleObject.minimum(:price).round
	end

	def getMaxPrice
		SaleObject.maximum(:price).round
	end

	def getNews	
		NewsOnSite.is_public.limit(3).order("created_at DESC")
	end	

	def pointer
		image_tag "pointer.png"
	end

	def getTestButton
		image_tag "test.png"
	end

	def getQuestionButton
		image_tag "vopros.png"
	end

	def getRequestButton
		image_tag "zayava.png"
	end

	def pen
		image_tag "pen.png"
	end	

	def breadcrambs page_name
	 content_for :breadcrambs do
 		"#{link_to 'Главная', root_path} &nbsp; <div class='current_page'> #{page_name}</div>".html_safe
   end
	end

	def sortable(column, title = nil)
  	title ||= column.titleize
  	css_class = column == sort_column ? "#{sort_direction}C" : 'descC'
  	direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
  	"<span class=#{css_class}>#{link_to title, params.merge(sort: column, direction: direction)}</span>".html_safe
	end

	def meta id_page
		title = ""
		description = ""
		keywords = ""
		if Seo.exists?(id_page)
			seo = Seo.find id_page 
			title = " - #{seo.title}".html_safe
			description = seo.description.html_safe
			keywords = seo.keywords.html_safe		
		end			
 		metamagic title: "Агентство недвижимости \"КИРШ\"#{title}", description: description, keywords: keywords
 	end
end
