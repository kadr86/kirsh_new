# encoding: utf-8
module SessionsHelper
	
  def sign_in(user)
    	cookies.permanent.signed[:remember_token] = [user.id, user.salt]
      self.current_user
  end

  def sign_out
    cookies.delete(:remember_token)    
  end

  def current_user
    @current_user ||= user_from_remember_token
  end

  def signed_in?
    !current_user.nil?
  end
  
  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    clear_return_to
  end

  def current_user?(user)
      user == current_user
  end

  def deny_access
      store_location
      redirect_to signin_path, notice: "Войдите в систему под своей учетной записью."
  end 

  def admin_access
    store_location
    _403
  end

  def admin?
    current_user.role.id == 1
  end

  def user?
    current_user.role.id == 2
  end
#Приватные методы
private

    def store_location
      session[:return_to] = request.fullpath
    end

    def clear_return_to
      session[:return_to] = nil
    end    

    def user_from_remember_token
      UserAccount.authenticate_with_salt(*remember_token)
    end

    def remember_token
      cookies.signed[:remember_token] || [nil, nil]
    end
end