# encoding: utf-8

class SaleObjectsController < ApplicationController
    
  before_filter only: [:new, :edit, :create, :update] do 
    @personals = {}
    @regions = {}
    @districts = []
    Personal.where(status: 1).order('sename ASC').each {|porsona| @personals["#{porsona.sename} #{porsona.name}"] = porsona.id }
    Region.all.order('name ASC').each { |region|  @regions[region.name] = region.id}    
    region = Region.first
    if !region.nil? && region.districts.any?
     region.districts.each do |district|
      @districts << district.name
     end
    end
  end

   before_filter only: [:index, :filter] do
    
    @streets = []
    SaleObject.all.order("street ASC").group(:street).each { |street|  @streets << street.street}    
  end  

  def index      
      @sale_objects = SaleObject.all   
      @count = @sale_objects.count
  end

  def new    
  	@sale_object = SaleObject.new
    # 3.times {@sale_object.photos.build   }
  	# @sale_object.photo = params[:file]
  end

  def create
    # render text: post_params
    @sale_object = SaleObject.new(
      region: params[:sale_object][:region],
      locality_type: params[:sale_object][:locality_type],
      locality: params[:sale_object][:locality],
      area: params[:sale_object][:area],
      district: params[:sale_object][:district],
      street_type: params[:sale_object][:street_type],
      street: params[:sale_object][:street],
      object_type: params[:sale_object][:object_type],
      object: params[:sale_object][:object],
      letter: params[:sale_object][:letter],
      block_type: params[:sale_object][:block_type],
      block: params[:sale_object][:block],
      flat: params[:sale_object][:flat],
      index: params[:sale_object][:index],
      station_metro: params[:sale_object][:station_metro],
      agency: params[:sale_object][:agency],
      personal_id: params[:sale_object][:personal_id],
      status: params[:sale_object][:status],
      begin_contract: params[:sale_object][:begin_contract],
      end_contract: params[:sale_object][:end_contract],
      owner: params[:sale_object][:owner],
      owner_phone: params[:sale_object][:owner_phone],
      price: params[:sale_object][:price],
      hide_price: params[:sale_object][:hide_price],
      client_request: params[:sale_object][:client_request],
      client_phone: params[:sale_object][:client_phone],
      finaly_price: params[:sale_object][:finaly_price],
      date: params[:sale_object][:date],
      structure_type: params[:sale_object][:structure_type],
      nswe: params[:sale_object][:nswe],
      rooms_count: params[:sale_object][:rooms_count],
      general_square: params[:sale_object][:general_square],
      live_square: params[:sale_object][:live_square],
      kittchen_square: params[:sale_object][:kittchen_square],
      area_square: params[:sale_object][:area_square],
      sanysel: params[:sale_object][:sanysel],
      material: params[:sale_object][:material],
      state: params[:sale_object][:state],
      new_old: params[:sale_object][:new_old],
      land: params[:sale_object][:land],
      owner_form: params[:sale_object][:owner_form],
      floor: params[:sale_object][:floor],
      all_floors: params[:sale_object][:all_floors],
      balkon: params[:sale_object][:balkon],
      lodjiya: params[:sale_object][:lodjiya],
      parking: params[:sale_object][:parking],
      garage: params[:sale_object][:garage],
      lift: params[:sale_object][:lift],
      phone_line: params[:sale_object][:phone_line],
      description: params[:sale_object][:description],
      agreement: params[:sale_object][:agreement],
      id: params[:id])
    if @sale_object.save
      10.times do |i|
       i += 1
       unless  params[:sale_object]['photo'+i.to_s].nil? 
        photo = params[:sale_object]['photo'+i.to_s][0]
        @sale_object.photo_upload photo, @sale_object.id
        Photo.create(sale_object_id: @sale_object.id, photo: photo.original_filename)
       end
      end    
      redirect_to sale_objects_path, notice: "Объект отредактирован"
     else
      render 'new'
     end      
  end

  def edit
     @sale_object = SaleObject.find params[:id]     
     @sale_object.object.gsub!(/[А-Я]*[а-я]*/,"")
     @sale_object.block.gsub!(/[А-Я]*[а-я]*\./,"")  
     # 3.times {@sale_object.photos.build   }     
  end

  def update    
    @sale_object = SaleObject.find params[:id]     
    if @sale_object.update(
      region: params[:sale_object][:region],
      locality_type: params[:sale_object][:locality_type],
      locality: params[:sale_object][:locality],
      area: params[:sale_object][:area],
      district: params[:sale_object][:district],
      street_type: params[:sale_object][:street_type],
      street: params[:sale_object][:street],
      object_type: params[:sale_object][:object_type],
      object: params[:sale_object][:object],
      letter: params[:sale_object][:letter],
      block_type: params[:sale_object][:block_type],
      block: params[:sale_object][:block],
      flat: params[:sale_object][:flat],
      index: params[:sale_object][:index],
      station_metro: params[:sale_object][:station_metro],
      agency: params[:sale_object][:agency],
      personal_id: params[:sale_object][:personal_id],
      status: params[:sale_object][:status],
      begin_contract: params[:sale_object][:begin_contract],
      end_contract: params[:sale_object][:end_contract],
      owner: params[:sale_object][:owner],
      owner_phone: params[:sale_object][:owner_phone],
      price: params[:sale_object][:price],
      hide_price: params[:sale_object][:hide_price],
      client_request: params[:sale_object][:client_request],
      client_phone: params[:sale_object][:client_phone],
      finaly_price: params[:sale_object][:finaly_price],
      date: params[:sale_object][:date],
      structure_type: params[:sale_object][:structure_type],
      nswe: params[:sale_object][:nswe],
      rooms_count: params[:sale_object][:rooms_count],
      general_square: params[:sale_object][:general_square],
      live_square: params[:sale_object][:live_square],
      kittchen_square: params[:sale_object][:kittchen_square],
      area_square: params[:sale_object][:area_square],
      sanysel: params[:sale_object][:sanysel],
      material: params[:sale_object][:material],
      state: params[:sale_object][:state],
      new_old: params[:sale_object][:new_old],
      land: params[:sale_object][:land],
      owner_form: params[:sale_object][:owner_form],
      floor: params[:sale_object][:floor],
      all_floors: params[:sale_object][:all_floors],
      balkon: params[:sale_object][:balkon],
      lodjiya: params[:sale_object][:lodjiya],
      parking: params[:sale_object][:parking],
      garage: params[:sale_object][:garage],
      lift: params[:sale_object][:lift],
      phone_line: params[:sale_object][:phone_line],
      description: params[:sale_object][:description],
      agreement: params[:sale_object][:agreement],
      id: params[:id])
      10.times do |i|
       i += 1
       unless  params[:sale_object]['photo'+i.to_s].nil? 
        photo = params[:sale_object]['photo'+i.to_s][0]
        @sale_object.photo_upload photo, @sale_object.id
        Photo.create(sale_object_id: @sale_object.id, photo: photo.original_filename)
       end
      end    
      redirect_to sale_objects_path, notice: "Объект отредактирован"
     else
      render 'edit'
     end      
  end

  def show
  end

  def destroy
    if SaleObject.find(params[:id]).destroy
      redirect_to sale_objects_path, notice: "Объект был удален"
    else
      redirect_to sale_objects_path, notice: "Объект не был удален"
    end
  end

  def hot_offer
    offer = SaleObject.find params[:id_ob]
    offer.update(hot_offer: params[:val])  
    render nothing: true       
  end

  def avans
    sale_object = SaleObject.find(params[:id])
    if sale_object.update(avans: true, sold: false, expired: false, status: 5)
      effective_rieltors sale_object
      redirect_to edit_sale_object_path(params[:id])
    else
      render 'edit'
    end
  end

  def sold
    sale_object = SaleObject.find(params[:id])
    if sale_object.update(sold: true, expired: false, avans: false, status: 6)
      effective_rieltors sale_object
      redirect_to edit_sale_object_path(params[:id])
    else
      render 'edit'
    end
  end

  def expired
    sale_object = SaleObject.find(params[:id])
    if sale_object.update(expired: true, avans: false, sold: false, status: 7)  
      effective_rieltors sale_object
      redirect_to edit_sale_object_path(params[:id])
    else
      render 'edit'
    end
  end

  def photo_delete
    photo = Photo.find(params[:file_id])
    if photo.destroy
      File.delete(Rails.root.join('public','sale_objects',photo.sale_object_id.to_s,"small_#{photo.photo}"))
      File.delete(Rails.root.join('public','sale_objects',photo.sale_object_id.to_s,"medium_#{photo.photo}"))
      File.delete(Rails.root.join('public','sale_objects',photo.sale_object_id.to_s,"original_#{photo.photo}"))
      redirect_to edit_sale_object_path(photo.sale_object_id), notice: "Фотография была удалена"
    else
      redirect_to edit_sale_object_path(photo.sale_object_id), notice: "Фотография не была удалена"
    end
  end

   def getDistricts
    @districts = []
    region_id = params[:region_id]
    region_id = 1 if params[:region_id].nil?
    region = Region.find(region_id)
    if region.districts.any?
     region.districts.order("name ASC").each do |district|
      @districts << "<option value='#{district.name}'>#{district.name}</option>"
     end
    end    
    render text: @districts.join(' ')
  end

 def filter
   from = params[:filter][:from]
   to = params[:filter][:to]
   structure_type = params[:filter][:structure_type]   
   status = params[:filter][:status]
   street = params[:filter][:street]
   rooms_count = params[:filter][:rooms_count]
   query = {}
   query[:begin_contract] = [from..to] unless from.blank? && to.blank?    
   query[:structure_type] = structure_type unless structure_type.blank?
   query[:status] = status unless status.blank?   
   query[:street] = street unless street.blank?
   query[:rooms_count] = rooms_count unless rooms_count.blank?

   @sale_objects = SaleObject.where(query)
   @count = @sale_objects.count
   render 'index'
  end

private

  def post_params
    params.require(:sale_object).permit! #( :region, :locality_type, :locality, :area, :district, :street_type, :street, :object_type, :object, :letter, :block_type, :block, :flat, :index, :station_metro, :agency, :personal_id, :status, :begin_contract, :end_contract, :owner, :owner_phone, :price, :hide_price, :client_request, :client_phone, :finaly_price, :date, :structure_type, :nswe, :rooms_count, :general_square, :live_square, :kittchen_square, :area_square, :sanysel, :material, :state, :floor, :all_floors, :balkon, :lodjiya, :parking, :garage, :lift, :phone_line, :description, :agreement, :id)
  end

  def effective_rieltors object
    EffectivRieltor.create(personal_id: object.personal_id, sale_object_id: object.id, status: object.status, agreement: object.agreement)
  end

end
