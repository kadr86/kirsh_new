class UserAccountsController < ApplicationController
	skip_before_filter :autentificate, :only => [:new, :create]

  before_filter only:[:edit,:show,:update] do
    @user = UserAccount.find(params[:id])
  end
    
  def index
  end

  def new
  	@user = UserAccount.new
  end

  def create
    if @user = UserAccount.create(post_params)  	
  	  redirect_to signin_path
  	else
  		render 'new'  
  	end
  end

  def show
  end

  def edit
  end

  def update
    if @user.update(post_params)    
      redirect_to user_account_path(@user)
    else
      render 'edit'
    end
  end
  

  private
  
  def post_params 
    params.require(:user_account).permit(:email, :login,:encrypted_password, :password, :password_confirmation)
  end

end
  

