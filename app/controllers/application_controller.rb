class ApplicationController < ActionController::Base   
 
  include SessionsHelper
  include SimpleCaptcha::ControllerHelpers

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
	before_filter :autentificate
  layout 'application'   


  

	private
  
	def autentificate
  	deny_access unless signed_in?
  end

  def admin_only
  	admin_access unless signed_in? && admin?
  end  

  



  
  
end
