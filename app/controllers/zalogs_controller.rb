class ZalogsController < ApplicationController
  def edit
  	@zalog = Zalog.take
  end

  def update
  	@zalog = Zalog.find params[:id]
  	if @zalog.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@zalog = Zalog.new
  end

  def create  	
  	if @zalog = Zalog.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:zalog).permit!  	
 end  
end

