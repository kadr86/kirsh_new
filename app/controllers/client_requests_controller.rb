# encoding: utf-8
class ClientRequestsController < ApplicationController
  include SaleObjectsHelper

  before_filter only: [:index, :edit, :new, :load_request] do 
    @personals = {}    
    Personal.where(status: 1).order("sename ASC").each { |persona| @personals["#{persona.sename} #{persona.name}"] = persona.id}
  end  

  before_filter only: [:new, :edit] do
    
    @regions = {}
    @districts = []
    @objects = SaleObject.where.not(status:[6,7])
    Region.all.each {|region| @regions[region.name] = region.id}
    if Region.exists? 1
      region = Region.find 1
      if region.districts.any?
       region.districts.order("name ASC").each do |district|
        @districts << district.name
       end
      end
    end
  end
  


  def index  	    
    @client_requests = ClientRequest.all
    @count = @client_requests.count
  end

  def new
    @client_request = ClientRequest.new  
    @client_request.build_client
  end

  def create
    @client_request = ClientRequest.new(post_params)      
    if @client_request.save()     
      redirect_to client_requests_path, notice: "Проспект добавлен"
    else
      render 'new'
    end
  end

  def edit
    @client_request = ClientRequest.find params[:id] 
  end

  def update
    @client_request = ClientRequest.find params[:id] 
    if @client_request.update(post_params)
      redirect_to client_requests_path, notice: "Проспект отредактирован"
    else
      render 'edit'
    end
  end

  def show
  end

  def load_request
   
   from = params[:filter][:from]
   to = params[:filter][:to]
   type = params[:filter][:type]
   rooms_count = params[:filter][:rooms_count]
   status = params[:filter][:status]
   rieltor = params[:filter][:rieltor]
   query = {}
   query[:created_at] = [from..to] unless from.blank? && to.blank?
   query[:structure_type] = type unless type.blank?
   query[:status] = status unless status.blank?
   query[:rooms_count] = rooms_count unless rooms_count.blank?   
   query[:personal_id] = rieltor unless rieltor.blank? 

   @client_requests = ClientRequest.includes(:sale_object).where(query)
   @count = @client_requests.count 
   render 'index'
  end

  def check_phone
    if Client.where(phone: params[:phone]).take
      render text: 1
    else
      render text: 0
    end
  end

  def destroy
    if ClientRequest.find(params[:id]).destroy
      redirect_to client_requests_path, notice: "Проспект удален"
    else
      redirect_to client_requests_path, notice: "Проспект не удален"
    end
  end

private
  
  def post_params
    params.require(:client_request).permit!    
  end    
end
