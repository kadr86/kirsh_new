class EffectiveRieltorsController < ApplicationController
  def index
    @results = {}     
  	if params[:filter].presence
     @from = params[:filter][:from]
     @to = params[:filter][:to]   	 
     @effective = Personal.where(id: SaleObject.select(:personal_id).where(begin_contract: @from..@to)).order(:sename) unless params[:status].present?
     @effective = Personal.where(id: SaleObject.select(:personal_id).where(begin_contract: @from..@to)).where(status: 1).order(:sename) if params[:status].present?

  	else        	 
     @effective = Personal.all.order(:sename) unless params[:status].present?
     @effective = Personal.where(status: 1).order(:sename) if params[:status].present?
  	end    
  end
  
  def new
  end

  def edit
  end

  def show
    if params[:filter].presence
     from = params[:filter][:from]
     to = params[:filter][:to]
     active = params[:filter][:active].to_i  
     filter = {}
     filter[:begin_contract] = [from..to] unless from.blank? && to.blank?
     filter[:status] = [3,4] unless active.zero?    
    end
   @rieltor = SaleObject.where(personal_id: params[:id]).where(filter)
  end

end
