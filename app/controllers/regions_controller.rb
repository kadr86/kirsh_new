class RegionsController < ApplicationController
  def index
  	@regions = Region.all
  end


  def new
  	@region = Region.new
  end

  def create
  	if @region = Region.create(params_hash)
  		redirect_to regions_path
  	else
  		render 'new'
  	end
  end

  def edit
  	@region = Region.find params[:id]  	
  end

  def update
  	@region = Region.find params[:id]
  	if @region.update(params_hash)
  		redirect_to regions_path
  	else
  		render 'edit'
  	end
  end

  def destroy
    Region.find(params[:id]).destroy
    redirect_to regions_path
  end


private

 def params_hash
 	params.require(:region).permit(:name)  	
 end  
end
