class NewsOnSitesController < ApplicationController
  skip_before_filter :autentificate, only:[:index, :show]

  def index
  	@news = NewsOnSite.is_public.order('created_at DESC').page(params[:page]).per(6)
    @id_page = 17
    render layout: 'site'
  end


  def admin_index
    @news = NewsOnSite.all
  end

  def new
  	@new = NewsOnSite.new
  end

  def create
  	if @new = NewsOnSite.create(params_hash)
  		redirect_to admin_index_news_on_sites_path
  	else
  		render 'new'
  	end
  end

  def edit
  	@new = NewsOnSite.find params[:id]  	
  end

  def update
  	@new = NewsOnSite.find params[:id]
  	if @new.update(params_hash)
  		redirect_to admin_index_news_on_sites_path
  	else
  		render 'edit'
  	end
  end

  def show
    @new = NewsOnSite.find(params[:id])
    @id_page = 18
    render layout: 'site'
  end

  def public
    news = NewsOnSite.find(params[:id])
    news.update(is_public: params[:status])
    redirect_to admin_index_news_on_sites_path    
  end

  def destroy
    NewsOnSite.find(params[:id]).destroy
    redirect_to admin_index_news_on_sites_path    
  end

private

 def params_hash
 	params.require(:news_on_site).permit!  	
 end  
end
