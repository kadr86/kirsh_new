# encoding: utf-8

class PapersController < ApplicationController
	skip_before_filter :autentificate, only: :site_index

  def index
  	@papers = Paper.all.order(created_at: :desc).page(params[:page]).per(30)
  end

  def site_index
  	@papers = Paper.all.order(created_at: :desc).page(params[:page]).per(50)
  	render layout: 'site'
  end

  def new
  	@paper = Paper.new
  end

  def create
  	@paper = Paper.new(post_params)
  	if @paper.save
  		redirect_to papers_path
  	else
  		render :new
  	end
  end

  def edit
  	@paper = Paper.find params[:id]  	
  end
	
	def update
		@paper = Paper.find params[:id]
  	if @paper.update(post_params)  	
  		redirect_to papers_path
  	else
  		render :edit
  	end
	end

	def destroy
		if Paper.find(params[:id]).destroy
			redirect_to papers_path, notice: "Удалено"
		else
			redirect_to papers_path, notice: "Не удалено"
		end

	end

	private

	def post_params
		params.require(:paper).permit!
	end

end
