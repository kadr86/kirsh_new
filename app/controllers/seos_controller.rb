class SeosController < ApplicationController
  def index
  	@seos = Seo.all
  end

  def new
  	@seo = Seo.new
  end

  def create
  	@seo = Seo.new post_params
  	if @seo.save
  		redirect_to seos_path
  	else
  		redirect_to seos_path
  	end
  end

  def edit
  	@seo = Seo.find  params[:id]
  end

  def update
  	@seo = Seo.find  params[:id]
  	if @seo.update post_params
  		redirect_to seos_path
  	else
  		redirect_to seos_path
  	end
  end

  private 

  def post_params
  	params.require(:seo).permit!
  end
end
