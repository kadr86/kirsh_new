class OwnerKomerchesController < ApplicationController
 def edit
  	@owner_komerch = OwnerKomerch.find params[:id]
  end

  def update
  	@owner_komerch = OwnerKomerch.find params[:id]
  	if @owner_komerch.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@owner_komerch = OwnerKomerch.new
  end

  def create  	
  	if @owner_komerch = OwnerKomerch.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:owner_komerch).permit!  	
 end  

end


