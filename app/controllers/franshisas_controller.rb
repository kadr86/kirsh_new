class FranshisasController < ApplicationController
  def edit
  	@franshisa = Franshisa.take
  end

  def update
  	@franshisa = Franshisa.find params[:id]
  	if @franshisa.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@franshisa = Franshisa.new
  end

  def create  	
  	if @franshisa = Franshisa.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:franshisa).permit!  	
 end  
end

