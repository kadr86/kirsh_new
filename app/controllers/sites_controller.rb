# encoding: utf-8

class SitesController < ApplicationController
	skip_before_filter :autentificate
	helper_method :sort_column, :sort_direction
	include SaleObjectsHelper
	layout 'site'

	before_filter only: [:index, :search, :search_request, :search_map] do
		@regions = {}
		@streets = []
		@districts = []
		Region.all.each { |region| @regions[region.name] = region.id}
		SaleObject.all.order('street ASC').group('street').each {|street| @streets << street.street}

		region = Region.first
    if !region.nil? && region.districts.any?
     region.districts.each do |district|
      @districts << district.name
     end
    end
	end

	before_filter only:[:question_answer, :question_answer_create] do
		@questions = Question.all.order("created_at DESC").public
	end

	def index
		@text = MainPage.take
		# @adress = []
		# SaleObject.all.each {|adress| @adress << "#{adress.id}:#{adress.locality} #{adress.street_type}#{adress.street},#{adress.object_type}.#{adress.object}#{adress.letter} #{adress.block_type unless adress.block.blank?}#{adress.block}".strip}
		# @adress = @adress.join(';')
		@adress_lived = []
     	@adress_commerc = []
      SaleObject.where(structure_type: getTypeLived).each do |adress|      	
    	@adress_lived << "#{adress.id}:#{adress.locality} #{adress.street_type}#{adress.street},#{adress.object_type}.#{adress.object}#{adress.letter} #{adress.block_type unless adress.block.blank?}#{adress.block}".strip
      end	 	
      SaleObject.where(structure_type: getTypeCommerc).each do |adress|      	
       	@adress_commerc << "#{adress.id}:#{adress.locality} #{adress.street_type}#{adress.street},#{adress.object_type}.#{adress.object}#{adress.letter} #{adress.block_type unless adress.block.blank?}#{adress.block}".strip
      end
       @adress_lived = @adress_lived.join(';')
       @adress_commerc = @adress_commerc.join(';')
	   @id_page = 1
	   @top_banners = TopBanner.is_public.all
	end

	def about
		@about = About.take
		@id_page = 2
	end

	def question_answer
		@question = Question.new
		@id_page = 13
	end

	def question_answer_create
		 @question = Question.new(question_params)
		if  @question.valid_with_captcha?	&& @question.save
			QuestionMailer.new_question(@question.name, @question.id).deliver
			redirect_to question_answer_sites_path
		else
			render "question_answer"
		end
	end

	def search
		status = params[:status] if params[:status].presence
		status = 3 unless params[:status].presence
		@objects = SaleObject.is_public.includes(:personal).where(status: status).order(sort_column + " " + sort_direction)
		@id_page = 3
	end

	def object_detail
		@sale_object = SaleObject.find params[:id]
		@id_page = 4
	end

	def search_request
		offer = params[:search_form][:offer]
		region = params[:search_form][:region]
		object = params[:search_form][:object]
		district = params[:search_form][:district]
		street = params[:search_form][:street]
		rooms = params[:search_form][:rooms]
		new_old = params[:search_form][:new_old]
		square_from = params[:search_form][:square_from]
		square_to = params[:search_form][:square_to]
		price_from = params[:search_form][:price_from]
		price_to = params[:search_form][:price_to]
		@sort = params[:search_form][:sort]
		field_sort = params[:search_form][:field_sort]
		query = {}
		query[:status] = offer unless offer.blank?
		query[:region] = region unless region.blank?
		query[:structure_type] = object unless object.blank?
		query[:district] = district unless district.blank?
		query[:street] = street unless street.blank?
		query[:rooms_count] = rooms
		query[:new_old] = new_old unless new_old.blank?
		query[:general_square] =	([square_from..square_to] unless square_from.blank? && square_to.blank?) #unless object == 'Зем. участки'
		query[:price] = [price_from..price_to] unless price_from.blank? && price_to.blank?

	  @objects = SaleObject.where(query).order("#{field_sort} #{@sort}")
		render 'search'
	end

 def search_map
    @text = MainPage.take
    offer = params[:search_form][:offer]
    region = params[:search_form][:region]
    object = params[:search_form][:object]
    district = params[:search_form][:district]
    street = params[:search_form][:street]
    rooms = params[:search_form][:rooms]
    square_from = params[:search_form][:square_from]
    square_to = params[:search_form][:square_to]
    price_from = params[:search_form][:price_from]
    price_to = params[:search_form][:price_to]

    if rooms.to_i == 0
      @objects = SaleObject.no_rooms(offer,square_from, square_to, price_from, price_to, region, object)
      @objects = @objects.district(district) unless district.blank?
      @objects = @objects.street(street) unless street.blank?
    else
      @objects = SaleObject.rooms(offer,rooms, square_from, square_to, price_from, price_to, region, object)
      @objects = @objects.district(district) unless district.blank?
      @objects = @objects.street(street) unless street.blank?
    end
     # @adress_lived = []
     # @adress_commerc = []
     # @objects.each do |adress|
     #  if getTypeLived.include(adress.structure_type)
     # 	@adress_lived << "#{adress.id}:#{adress.locality} #{adress.street_type}#{adress.street},#{adress.object_type}.#{adress.object}#{adress.letter} #{adress.block_type unless adress.block.blank?}#{adress.block}".strip}
     #  end	
     #  if getTypeCommerc.include(adress.structure_type)
     #   	@adress_commerc << "#{adress.id}:#{adress.locality} #{adress.street_type}#{adress.street},#{adress.object_type}.#{adress.object}#{adress.letter} #{adress.block_type unless adress.block.blank?}#{adress.block}".strip}
     #  end	
     # end
     # @adress_lived = @adress_lived.join(';')
     # @adress_commerc = @adress_commerc.join(';')
     render 'index'
 end


	def request_user
		@id_page = 5
	end

	def send_request
		case params[:request_form][:offer].to_i
		 when 3 then offer = 'Снять'
		 when 4 then offer = 'Купить'
		end
		name = params[:request_form][:fio]
		email = params[:request_form][:email]
		phone = params[:request_form][:phone]
		text = params[:request_form][:text]
		object = params[:request_form][:object]
		unless name.blank? or phone.blank? or text.blank? or not simple_captcha_valid?
			unless object.nil?
   			if object.size < 5000000
   			  QuestionMailer.new_user_request(offer, name, email, phone, text, object).deliver
   			else
   				@error = "Превышен допустимый размер файла вложения. Максимальный размер 5Мб."
   				render 'request_user' and return
   			end
   		else
   			QuestionMailer.new_user_request(offer, name, email, phone, text).deliver
   		end
			 redirect_to request_user_sites_path
		else
			render 'request_user'
		end
	end

	def request_advert
		@id_page = 6
	end

	def send_request_advert
		name = params[:request_form][:fio]
		email = params[:request_form][:email]
		phone = params[:request_form][:phone]
		text = params[:request_form][:text]
		unless name.blank? or phone.blank? or text.blank? or not simple_captcha_valid?
   		QuestionMailer.new_advert_request(name, email, phone, text).deliver
			redirect_to request_advert_sites_path
		else
			render 'request_advert'
		end
	end

	def contacts
		@consultants = Personal.where(position: [14,15], status: 1) #Консультанты по недвижимости
		@inovations = Personal.where(position: [9,10,19,21,22], status: 1) #Отдел новаций
		@komerchs = Personal.where(position: [11,12,13], status: 1) #Отдел комерческой недвижимости
		@administrativs = Personal.where(position: [1,2,3,4,5,6,7,8,16,17,18,20,23,24,25,26,27], status: 1) #Административный состав
		@id_page = 15
	end

	def zalog
	 	@zalog = Zalog.take
	 	@id_page = 7
	end

	def buyout
		@buyout = Buyout.take
		@id_page = 8
	end

	def kredit
		@kredit = Kredit.take
		@id_page = 10
	end

	def agents
		@agent = Agent.take
		@id_page = 11
	end

	def own_biznes
		@own_biznes = BisnesSelf.take
		@id_page = 1
	end

	def developer
		@developer = Developer.take
		@id_page = 9
	end

	def owner_komerch
		@owner_komerch = OwnerKomerch.take
		@id_page = 12
	end

	def print
		@sale_object = SaleObject.find params[:id]
		render 'object_detail', layout: "print"
	end

	def map_site
		@id_page = 16
	end

	def map_site_print
		render 'map_site', layout: "print"
	end

	def consultant
		name = params[:consultant_form][:name]
		email = params[:consultant_form][:email]
		question = params[:consultant_form][:question]
		QuestionMailer.consultant(name, email, question).deliver
		render nothing: true
	end

	def yamail
		render layout: 'yamail'
	end

  def document_example
@id_page = 22
  end


private

 def question_params
 	params.require(:question).permit!
 end

 def sort_column
    SaleObject.column_names.include?(params[:sort]) ? params[:sort] : "street"
 end

 def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
 end



end
