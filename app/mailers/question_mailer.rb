# encoding: utf-8

class QuestionMailer < ActionMailer::Base
  default from: "center@kirsh.ru"

  def new_question user, question_id
  	email = "marketing@kirsh.ru"
  	@user = user
  	@link = "http://kirsh.ru/answers/new?id=#{question_id}"
  	mail(to: email, subject: "Было добавлено новое сообщение на сайт.")
  end

  def new_user_request offer, name, user_email, phone, text, object = nil
  	email = "marketing@kirsh.ru"
    @offer = offer
    @name = name
    @email = user_email
    @phone = phone
    @text = text        
    attachments[object.original_filename] = object.read if object
    mail(to: email, subject: "Поступила заявка на с сайта.")
  end

  def new_advert_request name, user_email, phone, text
    email = "marketing@kirsh.ru"    
    @name = name
    @email = user_email
    @phone = phone
    @text = text            
    mail(to: email, subject: "Поступила заявка на размещение рекламы с сайта.")
  end

  def new_answer user_email
    @link = "http://kirsh.ru/sites/question_answer"        
    mail(to: user_email, subject: "Было добавлен ответ на ваше сообщение на сайте kirsh.ru")
  end

  def consultant name, user_email, question
    email = "marketing@kirsh.ru"
    @name = name
    @email = user_email    
    @question = question            
    mail(to: email, from: user_email,  subject: "Был задан вопрос с сайта.")
  end

  def test fio, email, phone, ball
    to = "marketing@kirsh.ru"
    @fio = fio
    @email = email
    @phone = phone
    @ball = ball
    mail(to: to, from: email, subject: "Результат теста с сайта kirsh.ru")
  end

end
