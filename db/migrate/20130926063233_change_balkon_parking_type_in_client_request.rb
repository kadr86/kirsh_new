class ChangeBalkonParkingTypeInClientRequest < ActiveRecord::Migration
  def change
  	change_column :client_requests, :balkon, :integer
  	change_column :client_requests, :parking, :integer
  end
end
