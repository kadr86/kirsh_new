class AddFunctionLineBuildingsCountOnSaleObjects < ActiveRecord::Migration
  def change  	
  	add_column :sale_objects, :line, :string
  	add_column :sale_objects, :buildings_count, :integer
  end
end
