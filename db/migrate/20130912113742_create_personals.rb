class CreatePersonals < ActiveRecord::Migration
  def change
    create_table :personals do |t|
      t.references :user_account
      t.string :name, null: false
      t.string :sename, null: false
      t.string :email, null: false
      t.string :job_phone
      t.string :mobile_phone
      t.string :official_adress
      t.string :current_adress
      t.string :pasport
      t.integer :sex, null: false
      t.date :birthday
      t.date :begining_kurs
      t.integer :status, null: false
      t.string :position, null: false
      t.string :agency, null: false
      t.date :begin_job, null: false
      t.date :end_job
      t.integer :rang, null: false
      t.text :comment

      t.timestamps
    end
  end
end
