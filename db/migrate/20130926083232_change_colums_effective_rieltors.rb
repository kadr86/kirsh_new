class ChangeColumsEffectiveRieltors < ActiveRecord::Migration
  def change
  	rename_column :effectiv_rieltors, :unsold_objects, :active_sold
  	rename_column :effectiv_rieltors, :active_objects, :active_arenda
  	rename_column :effectiv_rieltors, :rejected_objects, :avans
  	add_column :effectiv_rieltors, :sold, :string
  	add_column :effectiv_rieltors, :put, :string
  	add_column :effectiv_rieltors, :expired, :string
  end
end
