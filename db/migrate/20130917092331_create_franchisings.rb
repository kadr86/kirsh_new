class CreateFranchisings < ActiveRecord::Migration
  def change
    create_table :franchasings do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
