class CreateUserAccounts < ActiveRecord::Migration
  def change
    create_table :user_accounts do |t|
      t.references :role, default: 2
      t.string :login
      t.string :password
      t.string :email
      t.integer :active, default: 1

      t.timestamps
    end
    add_index :user_accounts, :role_id
  end
end
