class CreateEffectivRieltors < ActiveRecord::Migration
  def change
    create_table :effectiv_rieltors do |t|
      t.references :personal, null: false
      t.string :unsold_objects
      t.string :active_objects
      t.string :rejected_objects

      t.timestamps
    end
  end
end
