class CreateAbouts < ActiveRecord::Migration
  def change
    create_table :abouts do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
