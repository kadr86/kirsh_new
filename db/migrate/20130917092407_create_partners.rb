class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
