class ChangeAgentTypeInClientRequest < ActiveRecord::Migration
  def change
  	change_column :client_requests, :agent, :integer
  end
end
