class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
