class CreateMainPages < ActiveRecord::Migration
  def change
    create_table :main_pages do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
