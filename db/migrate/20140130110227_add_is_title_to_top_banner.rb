class AddIsTitleToTopBanner < ActiveRecord::Migration
  def change
    add_column :top_banners, :title, :string
  end
end
