class AddImageColumnToPersonal < ActiveRecord::Migration
  def change
    add_column :personals, :image, :string
  end
end
