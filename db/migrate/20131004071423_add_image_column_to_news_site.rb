class AddImageColumnToNewsSite < ActiveRecord::Migration
  def change
    add_column :news_sites, :image, :string
  end
end
