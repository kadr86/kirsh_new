class CreateNewsSites < ActiveRecord::Migration
  def change
    create_table :news_sites do |t|
      t.string :title, null: false
      t.string :anons
      t.text :description
      t.integer :is_public, null: false

      t.timestamps
    end
  end
end
