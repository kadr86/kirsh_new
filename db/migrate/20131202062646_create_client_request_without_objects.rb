class CreateClientRequestWithoutObjects < ActiveRecord::Migration
  def change
    create_table :client_request_without_objects do |t|    	
      t.string :name, null: false
      t.string :sename, null: false
      t.string :email, null: false
      t.string :phone, null: false
      t.integer :sex, null: false
      t.string :offer_type, null: false
      t.string :operator, null: false
      t.integer :access, null: false
      t.string :status, null: false
      t.string :agent, null: false
      t.string :financing, null: false      
      t.string :region, null: false
      t.string :locality
      t.string :area
      t.string :district
      t.string :station_metro
      t.string :structure_type, null: false
      t.integer :rooms_count
      t.float :min_square
      t.float :max_price
      t.integer :floor
      t.string :construction, null: false
      t.integer :balkon
      t.integer :parking
      t.text :comment
      t.timestamps
    end
  end
end
