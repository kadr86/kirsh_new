class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.text :question, null: false

      t.timestamps
    end
  end
end
