class CreateClientRequests < ActiveRecord::Migration
  def change
    create_table :client_requests do |t|
      t.references :client, index: true
      t.references :sale_object, index: true
      t.string :offer_type, null: false
      t.string :operator, null: false
      t.integer :access, null: false
      t.string :status, null: false
      t.string :agent, null: false
      t.string :financing, null: false      
      t.string :region, null: false
      t.string :locality
      t.string :area
      t.string :district
      t.string :station_metro
      t.string :structure_type, null: false
      t.integer :rooms_count
      t.float :min_square
      t.float :max_price
      t.integer :floor
      t.string :construction, null: false
      t.integer :balkon
      t.integer :parking
      t.text :comment

      t.timestamps
    end
  end
end
