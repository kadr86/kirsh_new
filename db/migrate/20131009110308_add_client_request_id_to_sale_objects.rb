class AddClientRequestIdToSaleObjects < ActiveRecord::Migration
  def change
  	add_column :sale_objects, :client_request_id, :integer
  	add_index :sale_objects, :client_request_id
  end
end
