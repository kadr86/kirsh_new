class CreateForeigns < ActiveRecord::Migration
  def change
    create_table :foreigns do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
