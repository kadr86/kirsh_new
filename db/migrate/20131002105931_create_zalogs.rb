class CreateZalogs < ActiveRecord::Migration
  def change
    create_table :zalogs do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
