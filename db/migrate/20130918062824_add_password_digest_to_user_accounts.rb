class AddPasswordDigestToUserAccounts < ActiveRecord::Migration
  def change
  	change_table :user_accounts do |t|
  		t.string :password_digest
  	end
  end
end
