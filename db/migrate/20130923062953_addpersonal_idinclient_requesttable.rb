class AddpersonalIdinclientRequesttable < ActiveRecord::Migration
  def change
  	add_column :client_requests, :personal_id, :integer
  	add_index :client_requests, :personal_id
  end
end
