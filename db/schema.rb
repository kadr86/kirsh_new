# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141006065657) do

  create_table "abouts", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "agents", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "answers", force: true do |t|
    t.integer  "question_id", null: false
    t.text     "answer",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree

  create_table "bisnes_selves", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "buyouts", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "client_request_without_objects", force: true do |t|
    t.string   "name",                       null: false
    t.string   "sename",                     null: false
    t.string   "email",                      null: false
    t.string   "phone",                      null: false
    t.integer  "sex",                        null: false
    t.string   "offer_type",                 null: false
    t.string   "operator",                   null: false
    t.integer  "access",                     null: false
    t.string   "agent",                      null: false
    t.string   "financing",                  null: false
    t.string   "region",                     null: false
    t.string   "locality"
    t.string   "area"
    t.string   "district"
    t.string   "station_metro"
    t.string   "structure_type",             null: false
    t.integer  "rooms_count"
    t.float    "min_square"
    t.float    "max_price"
    t.integer  "floor"
    t.string   "material",                   null: false
    t.integer  "balkon"
    t.integer  "parking"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "personal_id"
    t.string   "locality_type"
    t.date     "date_reg"
    t.integer  "status",         default: 1
  end

  create_table "client_requests", force: true do |t|
    t.integer  "client_id"
    t.integer  "sale_object_id"
    t.string   "offer_type",     null: false
    t.string   "operator",       null: false
    t.integer  "access",         null: false
    t.integer  "status",         null: false
    t.integer  "agent",          null: false
    t.string   "financing",      null: false
    t.string   "region",         null: false
    t.string   "locality"
    t.string   "area"
    t.string   "district"
    t.string   "station_metro"
    t.string   "structure_type", null: false
    t.integer  "rooms_count"
    t.float    "min_square"
    t.float    "max_price"
    t.string   "floor"
    t.string   "material",       null: false
    t.integer  "balkon"
    t.integer  "parking"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "personal_id"
    t.string   "locality_type"
    t.date     "date_reg"
  end

  add_index "client_requests", ["client_id"], name: "index_client_requests_on_client_id", using: :btree
  add_index "client_requests", ["personal_id"], name: "index_client_requests_on_personal_id", using: :btree
  add_index "client_requests", ["sale_object_id"], name: "index_client_requests_on_sale_object_id", using: :btree

  create_table "clients", force: true do |t|
    t.string   "name",       null: false
    t.string   "sename",     null: false
    t.string   "email"
    t.string   "phone",      null: false
    t.integer  "sex",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "developers", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "districts", force: true do |t|
    t.string   "name"
    t.integer  "region_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "effectiv_rieltors", force: true do |t|
    t.integer  "personal_id",                   null: false
    t.integer  "sale_object_id",    default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status"
    t.integer  "client_request_id", default: 0
    t.integer  "agreement"
  end

  add_index "effectiv_rieltors", ["sale_object_id"], name: "index_effectiv_rieltors_on_sale_object_id", using: :btree

  create_table "help_informations", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kredits", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "main_pages", force: true do |t|
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_on_sites", force: true do |t|
    t.string   "title",                   null: false
    t.text     "anons"
    t.text     "description"
    t.integer  "is_public",   default: 1, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
  end

  create_table "owner_komerches", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "papers", force: true do |t|
    t.string   "title"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "personals", force: true do |t|
    t.integer  "user_account_id"
    t.string   "name",            null: false
    t.string   "sename",          null: false
    t.string   "email"
    t.string   "job_phone"
    t.string   "mobile_phone"
    t.string   "official_adress"
    t.string   "current_adress"
    t.string   "pasport"
    t.integer  "sex",             null: false
    t.date     "birthday"
    t.date     "begining_kurs"
    t.integer  "status",          null: false
    t.string   "agency",          null: false
    t.date     "begin_job",       null: false
    t.date     "end_job"
    t.integer  "rang",            null: false
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar"
    t.integer  "position"
  end

  create_table "photos", force: true do |t|
    t.integer  "sale_object_id"
    t.string   "photo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "questions", force: true do |t|
    t.string   "name",                   null: false
    t.string   "email",                  null: false
    t.text     "question",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
    t.integer  "is_public",  default: 0
  end

  create_table "regions", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sale_objects", force: true do |t|
    t.integer  "personal_id",       default: 0
    t.integer  "sold",              default: 0
    t.integer  "expired",           default: 0
    t.integer  "avans",             default: 0
    t.integer  "region",                        null: false
    t.string   "locality",                      null: false
    t.string   "area"
    t.string   "district",                      null: false
    t.string   "street_type",                   null: false
    t.string   "street",                        null: false
    t.string   "object",                        null: false
    t.string   "letter"
    t.string   "block"
    t.integer  "flat"
    t.string   "index"
    t.string   "station_metro"
    t.string   "agency",                        null: false
    t.integer  "status",                        null: false
    t.date     "begin_contract",                null: false
    t.date     "end_contract"
    t.string   "owner"
    t.string   "owner_phone"
    t.float    "price",                         null: false
    t.integer  "hide_price",        default: 0
    t.string   "client_request"
    t.string   "client_phone"
    t.float    "finaly_price"
    t.date     "date"
    t.string   "structure_type",                null: false
    t.integer  "rooms_count"
    t.float    "general_square"
    t.float    "live_square"
    t.float    "kittchen_square"
    t.float    "area_square"
    t.string   "sanysel"
    t.string   "material"
    t.integer  "floor"
    t.integer  "all_floors"
    t.integer  "balkon",            default: 0
    t.integer  "lodjiya",           default: 0
    t.integer  "parking",           default: 0
    t.integer  "garage",            default: 0
    t.integer  "lift",              default: 0
    t.integer  "phone_line",        default: 0
    t.string   "nswe"
    t.text     "description"
    t.integer  "agreement",         default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "hot_offer",         default: 0
    t.string   "locality_type"
    t.string   "object_type"
    t.string   "block_type"
    t.string   "photo"
    t.integer  "client_request_id"
    t.string   "state"
    t.integer  "public"
    t.integer  "new_old"
    t.integer  "owner_form"
    t.string   "land"
    t.string   "function"
    t.string   "line"
    t.integer  "buildings_count"
  end

  add_index "sale_objects", ["client_request_id"], name: "index_sale_objects_on_client_request_id", using: :btree
  add_index "sale_objects", ["personal_id"], name: "index_sale_objects_on_personal_id", using: :btree

  create_table "select_objects", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seos", force: true do |t|
    t.string "page_name"
    t.string "title"
    t.string "description"
    t.string "keywords"
  end

  create_table "simple_captcha_data", force: true do |t|
    t.string   "key",        limit: 40
    t.string   "value",      limit: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_captcha_data", ["key"], name: "idx_key", using: :btree

  create_table "top_banners", force: true do |t|
    t.string   "page_url"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "is_public"
    t.string   "banner"
    t.string   "title"
  end

  create_table "user_accounts", force: true do |t|
    t.string   "login"
    t.string   "encrypted_password"
    t.string   "email"
    t.boolean  "active",             default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "salt"
    t.string   "password_digest"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "zalogs", force: true do |t|
    t.string   "title",       null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
